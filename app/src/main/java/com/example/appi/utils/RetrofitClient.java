package com.example.appi.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance = null;
    private Retrofit retrofit;

    private RetrofitClient() {
        retrofit = new Retrofit.Builder().baseUrl("https://fakestoreapi.com/products").addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static synchronized RetrofitClient getInstance(){
        if(instance == null)
            instance= new RetrofitClient();
        return instance;
    }

    public Retrofit getRetrofit(){return retrofit;}



}
