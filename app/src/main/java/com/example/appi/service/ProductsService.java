package com.example.appi.service;

import com.example.appi.entity.Products;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductsService {

    @GET("/products")
    Call<List<Products>> getProducts();

    @GET("/products/{id}")
    Call<List<Products>> getProductsById(@Path("id") int id);
}
