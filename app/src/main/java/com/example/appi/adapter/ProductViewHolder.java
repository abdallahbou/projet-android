package com.example.appi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appi.R;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private  final TextView productItemView;
    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);
        this.productItemView = itemView.findViewById(R.id.test);
    }

    public void bind(String text){productItemView.setText(text);}

    public static ProductViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_first, parent, false);
        return  new ProductViewHolder(view);
    }



}
