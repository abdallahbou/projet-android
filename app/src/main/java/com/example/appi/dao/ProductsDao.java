package com.example.appi.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.appi.entity.Products;

import java.util.List;

@Dao
public interface ProductsDao {

    @Insert
    void insert(Products products);


    @Transaction
    @Query("SELECT * FROM products")
    LiveData<List<Products>> getAll();


}
