package com.example.appi.utils;


import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.appi.dao.ProductsDao;
import com.example.appi.entity.Products;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Products.class},version = 2)
public abstract class ContextDataBase extends RoomDatabase{

    public abstract ProductsDao productsDao();


    public static ExecutorService databaseExecutor = Executors.newFixedThreadPool(3);


    @Override
    public void clearAllTables() {

    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(@NonNull DatabaseConfiguration databaseConfiguration) {
        return null;
    }
}

