package com.example.appi.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.appi.dao.ProductsDao;
import com.example.appi.entity.Products;
import com.example.appi.utils.ContextDataBase;

import java.util.List;

public class ProductsRepository {


    private ProductsDao _productsDao;
    public ProductsRepository(Application application){

    }

    public void insert(Products products)
    {
        ContextDataBase.databaseExecutor.execute(()->{
            _productsDao.insert(products);
        });
    }

    public LiveData<List<Products>> getAll() {
        return _productsDao.getAll();
    }

}

