package com.example.appi;

//import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appi.adapter.ProductListAdapter;
import com.example.appi.databinding.FragmentFirstBinding;
import com.example.appi.entity.Products;
import com.example.appi.repository.ProductsRepository;
import com.example.appi.service.ProductsService;
import com.example.appi.utils.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private ProductsService _productsService;
    private ProductsRepository _repository;
    private RecyclerView recyclerView;
    private ProductListAdapter adapter;
    private List<Products> productsList;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {



        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NavHostFragment.findNavController(FirstFragment.this)
//                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
//            }
//        });


        recyclerView = view.findViewById(R.id.test);
        adapter =new ProductListAdapter(new ProductListAdapter.ProductDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

//        _repository.getAll().observe(getActivity(), products -> {
//            adapter.submitList(products);
//        });

        _productsService = RetrofitClient.getInstance().getRetrofit().create(ProductsService.class);
        _productsService.getProducts().enqueue(new Callback<List<Products>>() {
            @Override
            public void onResponse(Call<List<Products>> call, Response<List<Products>> response) {
                productsList=response.body();
                adapter.submitList(productsList);
            }

            @Override
            public void onFailure(Call<List<Products>> call, Throwable t) {

                System.out.println(t.getMessage());
            }
        });

//        getDataFromApi();
    }

    private void getDataFromApi(){

        _productsService.getProducts().enqueue(new Callback<List<Products>>() {
            @Override
            public void onResponse(Call<List<Products>> call, Response<List<Products>> response) {
                if(response.isSuccessful()){
                    List<Products> products=response.body();
                    adapter.submitList(products);

                }
            }

            @Override
            public void onFailure(Call<List<Products>> call, Throwable t) {

                Log.e("Erreur connexion","error");
            }
        });

    }

//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        binding = null;
//    }

}